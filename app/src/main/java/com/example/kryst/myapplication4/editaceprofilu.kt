package com.example.kryst.myapplication4

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_editaceprofilu.*



class editaceprofilu : AppCompatActivity() {

    companion object {


    const val NAMEEDIT = "nameedit"
    const val LASTNAMEEDIT = "lastnameedit"
    const val POPISEDIT = "popisedit"
}
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_editaceprofilu)
        potvrditBtn.setOnClickListener{
            val intent = Intent(this, menuActivity::class.java)
            intent.putExtra(NAMEEDIT, nameEdit.toString())
            intent.putExtra(LASTNAMEEDIT, lastnameEdit.toString())
            intent.putExtra(POPISEDIT, popisEdit.toString())
            startActivity(intent)
        }
    }
}
